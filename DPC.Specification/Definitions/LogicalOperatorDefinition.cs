﻿namespace DPC.Specification.Definitions
{
    /// <summary>
    /// Definition of logical operator
    /// </summary>
    public class LogicalOperatorDefinition : BaseOperatorDefinition
    {
        public LogicalOperatorDefinition() : base()
        { 
        }
    }
}