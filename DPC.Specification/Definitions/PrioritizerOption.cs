﻿namespace DPC.Specification.Definitions
{
    /// <summary>
    /// Option of prioritizer
    /// </summary>
    public enum PrioritizerOption : int
    {
        BracketPrioritizer = 0,
        AutoPrioritizer = 1
    }
}