﻿namespace DPC.Specification.Definitions
{
    /// <summary>
    /// Defines predicate operator specific for language
    /// </summary>
    public class PredicateOperatorDefinition : BaseOperatorDefinition
    {
        public PredicateOperatorDefinition() : base()
        {
        }
    }
}